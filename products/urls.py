from django.conf.urls import url

from .views import (
    ProductListView,
    # product_list_view,
    # ProductDetailView,
    # product_detail_view,
    ProductDetailSlugView,
    SearchProductView,
    # ProductFeaturedListView,
    # ProductFeaturedDetailView
)

urlpatterns = [
    url(r'^$', ProductListView.as_view(), name='list'),
    url(r'^search$', SearchProductView.as_view(), name='search'),
    url(r'^(?P<slug>[\w-]+)$', ProductDetailSlugView.as_view(), name='product_detail'),
]
