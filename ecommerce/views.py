from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, get_user_model

from .forms import ContactForm

User = get_user_model()


def home_page(request):
    context = {
        'title': 'hello world',
        'content': 'Welcome to the ecommerce',
        'premium_content': 'YEAHHH!!'
    }

    return render(request, 'home_page.html', context)


def about_page(request):
    context = {
        'title': 'About'
    }
    return render(request, 'home_page.html', context)


def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    context = {
        'title': 'Contact',
        'form': contact_form,
    }
    if contact_form.is_valid():
        print(contact_form.cleaned_data)

    # if request.method == 'POST':
    #     print(request.POST.get('fullname'))

    return render(request, 'contact/view.html', context)

