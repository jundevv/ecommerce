import random
import string
from django.utils.text import slugify


def get_random_string(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def get_unique_order_id(instance, new_slug=None):
    order_id = get_random_string()

    klass = instance.__class__
    qs_exists = klass.objects.filter(order_id=order_id).exists()
    if qs_exists:
        return get_unique_order_id(instance, new_slug=new_slug)
    return order_id


def get_unique_slug(instance, new_slug=None):
    """
    This is for a Django project and it assumes your instance
    has a model with a slug field and a title character (char) field.
    """
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.title)

    klass = instance.__class__
    qs_exists = klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
                    slug=slug,
                    randstr=get_random_string(size=4)
                )
        return get_unique_slug(instance, new_slug=new_slug)
    return slug